FROM debian:latest

RUN apt update -y
RUN apt install apache2 libapache2-mod-php php php-mysql -y
RUN rm /var/www/html/index.html

COPY . /var/www/html/

ENV MYSQL_HOST=direccion_ip_contenedormysql
ENV MYSQL_USERNAME=root
ENV MYSQL_PASSWORD=12345
ENV MYSQL_DB=sistemas

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
